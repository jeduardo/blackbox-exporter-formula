# blackbox-exporter-formula


## A simple formula to configure a [Prometheus Blackbox Exporter](https://github.com/prometheus/blackbox_exporter).

Sample pillar:

```yaml
blackbox_exporter:
  system_username: blackbox
  version: 0.11.0
  archive_hash: sha256=6731efeb27d667ff5da362cc1afbb0a983d016697d906bb4b9c5503699efcbb7
  config_location: /etc/blackbox.yml
  config: |
    modules:
      http_2xx_no_verify:
        http:
          tls_config:
            insecure_skip_verify: false
```
